package ga.exdev.x1.main;

import javax.swing.ImageIcon;
import java.awt.*;

public class Background {

    private Image image = new ImageIcon(getClass().getResource("/background.png")).getImage();

    private int y = 0;
    private int yy = -720;

    public void draw(Graphics2D g) {

        g.drawImage(image, 0, y, null);
        g.drawImage(image, 0, yy, null);
    }

    public void move(int d) {

        y += d;
        yy += d;

        if (y == 720) {
            y = 0;
            yy = -720;
        }
    }
}
