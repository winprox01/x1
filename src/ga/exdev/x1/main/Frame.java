package ga.exdev.x1.main;

import ga.exdev.x1.Board;

import javax.swing.JFrame;

public class Frame extends JFrame {

    public Frame() {

        setTitle("X1 {v1.0 Build 060116}");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);;
        setSize(1280, 720);
        setResizable(false);
        add(new Board());
    }
}
