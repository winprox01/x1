package ga.exdev.x1;

import ga.exdev.x1.game.Player;
import ga.exdev.x1.title.Logo;

import javax.swing.*;
import java.awt.*;

public class Game {

    private int difficulty = 1;
    private int score;
    private int state = 0;

    private Image exdev = new ImageIcon(getClass().getResource("/exdev.png")).getImage();

    private Font font = new Font("Arial", Font.BOLD, 20);
    private Font introFont = new Font("DialogInput", Font.BOLD, 40);

    private Color menuColor = new Color(253, 129, 0);
    private Color visitColor = new Color(157, 158, 224);
    private Color annotationColor = new Color(105, 83, 166);

    private Logo logo;
    private Player player;

    public Game(Logo logo, Player player) {

        this.logo = logo;
        this.player = player;
    }

    //Getters & Setters
    public void setDifficulty(int difficulty) {

        this.difficulty = difficulty;
    }
    public void setScore(int score) {

        this.score = score;
    }
    public void setState(int state) {

        this.state = state;
    }
    public int getDifficulty() {

        return difficulty;
    }
    public int getState() {

        return state;
    }
    public Color getMenuColor() {

        return menuColor;
    }
    public String getNamedDifficulty() {

        String namedDifficulty = null;

        if (difficulty == 1) namedDifficulty = "Easy";
        if (difficulty == 2) namedDifficulty = "Medium";
        if (difficulty == 3) namedDifficulty = "Hard";
        if (difficulty == 4) namedDifficulty = "Impossible";

        return namedDifficulty;
    }

    public void draw(Graphics2D g) {

        //Intro
        if (state == 0) {

            g.setFont(introFont);

            g.setColor(annotationColor);
            g.drawString("An Independent Videogames Developer", 220, 525);

            g.setColor(visitColor);
            g.drawString("exdev.ga", 545, 230);

            g.drawImage(exdev, 470, 240, null);
        }

        //TitleScreen
        if (state == 1) {

            player.draw(g);
            logo.draw(g);

            g.setFont(font);
            g.setColor(menuColor);
            g.drawString("Visit [exdev.ga/x1] for game info & rules", 810, 20);
            g.drawString("Current Difficulty: ", 4, 20);

            if (difficulty == 1) g.drawString("Easy", 222, 20);
            if (difficulty == 2) g.drawString("Medium", 222, 20);
            if (difficulty == 3) g.drawString("Hard", 222, 20);
            if (difficulty == 4) g.drawString("Impossible", 222, 20);
        }

        //Running
        if (state == 2) {

            player.draw(g);
            logo.draw(g);

            g.setFont(font);
            g.setColor(menuColor);
            g.drawString(score + " pts.", 1, 21);
        }
    }

    public void move() {

        if (state == 1) {

            logo.moveIn();
            player.center();
        }

        if (state == 2) {

            logo.moveOut();
            player.moveX();
            player.moveY();
        }
    }
}
