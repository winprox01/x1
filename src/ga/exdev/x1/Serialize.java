package ga.exdev.x1;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

class Buffer implements Serializable {

    int bestScore;
    int speed;
    boolean arcade;
    String name;
    String time;
    String diff;

    Buffer(int bestScore, String name, String time, String diff, int speed, boolean arcade) {

        this.bestScore = bestScore;
        this.name = name;
        this.time = time;
        this.diff = diff;
        this.speed = speed;
        this.arcade = arcade;
    }
}

public class Serialize {

    public void write(int bestScore, String diff, int speed, boolean arcade) throws IOException {

        String name = System.getProperty("user.name");
        String time = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date());
        Buffer buffer = new Buffer(bestScore, name, time, diff, speed, arcade);

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("BestScore.x1"));
        out.writeObject(buffer);
        out.close();
    }

    public int readScore() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("BestScore.x1"));
        Buffer buffer = (Buffer) in.readObject();
        return buffer.bestScore;
    }
    public String readName() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("BestScore.x1"));
        Buffer buffer = (Buffer) in.readObject();
        return buffer.name;
    }
    public String readTime() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("BestScore.x1"));
        Buffer buffer = (Buffer) in.readObject();
        return buffer.time;
    }
    public  String readDifficulty() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("BestScore.x1"));
        Buffer buffer = (Buffer) in.readObject();
        return buffer.diff;
    }
    public  int readSpeed() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("BestScore.x1"));
        Buffer buffer = (Buffer) in.readObject();
        return buffer.speed;
    }
    public String readMode() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("BestScore.x1"));
        Buffer buffer = (Buffer) in.readObject();

        if (buffer.arcade)
            return "Arcade";
        else return "Simulator";
    }
}
