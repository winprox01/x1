package ga.exdev.x1.title;

import javax.swing.ImageIcon;
import java.awt.*;

public class Logo {

    private Image image = new ImageIcon(getClass().getResource("/logo.png")).getImage();
    private Image start = new ImageIcon(getClass().getResource("/start.gif")).getImage();

    private int x = 520;
    private int y = -250;
    private int start_x = 170;
    private int start_y = 800;

    public void draw(Graphics2D g) {

        g.drawImage(image, x, y, null);
        g.drawImage(start, start_x, start_y, null);
    }

    public void moveIn() {

        if (y < 220)
            y += 16;
        else y = 220;

        if (start_y > 500)
            start_y -= 10;
        else start_y = 500;
    }

    public void moveOut() {

        if (y > -250)
            y -= 16;
        else y = -250;

        if (start_y < 800)
            start_y += 10;
        else  start_y = 800;
    }
}
