package ga.exdev.x1;

import ga.exdev.x1.game.Alien;
import ga.exdev.x1.game.Bullet;
import ga.exdev.x1.game.Player;
import ga.exdev.x1.main.Background;
import ga.exdev.x1.title.Logo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Board extends JPanel implements ActionListener, Runnable {

    private boolean gamePaused = false;
    private boolean loadScores = false;

    private int score = 0;
    private int bestScore = 0;
    private int speed = 10;
    private int time = 0;
    private int bulletV = 10;
    private int startDifficulty = 0;
    private boolean arcade = true;
    private boolean addBullet = false;

    private List<Alien> aliens = new ArrayList<>();
    private List<Bullet> bullets = new ArrayList<>();

    private Timer main = new Timer(17, this);
    private Timer shoot = new Timer(400, new ActionListener() {

        public void actionPerformed(ActionEvent actionEvent) {

            if (game.getState() == 2 && arcade) {

                bullets.add(new Bullet(bulletV, player.getX(), player.getY()));
            }
        }
    });

    private Timer simulatorShoot = new Timer(300, new ActionListener() {

        public void actionPerformed(ActionEvent actionEvent) {

            if (game.getState() == 2 && !arcade && addBullet) {

                bullets.add(new Bullet(bulletV, player.getX(), player.getY()));
                addBullet = false;
            }
        }
    });

    private Background background = new Background();
    private Logo logo = new Logo();
    private Player player = new Player();
    private Thread EnemySpawn = new Thread(this);
    private Game game = new Game(logo, player);
    private Serialize serialize = new Serialize();

    private Font font = new Font("Arial", Font.ITALIC, 20);

    //Setter
    public void setGamePaused(boolean gamePaused) {

        this.gamePaused = gamePaused;
    }

    public Board() {

        main.start();
        shoot.start();
        simulatorShoot.start();
        EnemySpawn.start();

        addKeyListener(new MKA());
        setFocusable(true);
    }

    private class MKA extends KeyAdapter {

        public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            if (key == KeyEvent.VK_L && game.getState() == 1) loadScores = true;
                if (key == KeyEvent.VK_S && game.getState() == 1) try {
                    serialize.write(bestScore, game.getNamedDifficulty(), speed, arcade);
                    loadScores = true;
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            if (key == KeyEvent.VK_ENTER && player.getX() >= 560 && player.getX() <= 700
                    && player.getY() >= 360 && player.getY() <= 400) {
                game.setState(2);
                startDifficulty = game.getDifficulty();
            }

            if (key == KeyEvent.VK_LEFT && game.getState() == 2) player.setDx(-7);
            if (key == KeyEvent.VK_RIGHT && game.getState() == 2) player.setDx(7);
            if (key == KeyEvent.VK_UP && game.getState() == 2) player.setDy(-7);
            if (key == KeyEvent.VK_DOWN && game.getState() == 2) player.setDy(7);

            if (key == KeyEvent.VK_ESCAPE)
                if (game.getState() == 2) gotoTitle();
            else System.exit(0);

            if (key == KeyEvent.VK_1 && game.getState() == 1) {
                game.setDifficulty(1);
                bestScore = 0;
            }
            if (key == KeyEvent.VK_2 && game.getState() == 1) {
                game.setDifficulty(2);
                bestScore = 0;
            }
            if (key == KeyEvent.VK_3 && game.getState() == 1) {
                game.setDifficulty(3);
                bestScore = 0;
            }
            if (key == KeyEvent.VK_4 && game.getState() == 1) {
                game.setDifficulty(4);
                bestScore = 0;
            }

            if (key == KeyEvent.VK_SPACE && game.getState() == 2) {

                if (gamePaused) {
                    gamePaused = false;

                } else if (!gamePaused) {
                    gamePaused = true;
                }
            }

            if (key == KeyEvent.VK_T && game.getState() == 1) {

                if (arcade) {
                    arcade = false;

                } else if (!arcade) {
                    arcade = true;
                }

                bestScore = 0;
            }

            if (key == KeyEvent.VK_SHIFT && !arcade) {

                addBullet = true;
            }

            if (key == KeyEvent.VK_P && game.getState() == 1 && speed <= 98) {
                speed++;
                bestScore = 0;
            }
            if (key == KeyEvent.VK_O && game.getState() == 1 && speed >= 11) {
                speed--;
                bestScore = 0;
            }
        }

        public void keyReleased(KeyEvent e) {

            int key = e.getKeyCode();

            if (key == KeyEvent.VK_LEFT | key == KeyEvent.VK_RIGHT && game.getState() == 2) player.setDx(0);
            if (key == KeyEvent.VK_UP | key == KeyEvent.VK_DOWN && game.getState() == 2) player.setDy(0);
        }
    }

    public void run() {

        Random random = new Random();

        while (true) {

            try {

                int diff = 2000;

                if (game.getDifficulty() == 1) diff = 2000;
                if (game.getDifficulty() == 2) diff = 1000;
                if (game.getDifficulty() == 3) diff = 500;
                if (game.getDifficulty() == 4) diff = 250;

                Thread.sleep(random.nextInt(diff));
                if (!gamePaused) {

                    aliens.add(new Alien(random.nextInt(1250), -64, 5 + random.nextInt(speed), player));
                }

            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }

    public void actionPerformed(ActionEvent e) {

        if (score >= 500 && game.getDifficulty() == 1)
            game.setDifficulty(2);
        if (score >= 1000 && game.getDifficulty() == 2)
            game.setDifficulty(3);
        if (score >= 2000 && game.getDifficulty() == 3)
            game.setDifficulty(4);

            testCollision();

        if (!gamePaused) {

            game.move();
            background.move(5);
            game.setScore(score);
        }

        if (game.getState() == 0 && time <= 200)
            time++;
        else if (game.getState() == 0 && time >= 200) game.setState(1);

        repaint();
    }

    private void testCollision() {

        Iterator<Alien> alienIterator = aliens.iterator();

        try {

            while (alienIterator.hasNext()) {

                Alien alien = alienIterator.next();

                if (player.getRectangle().intersects(alien.getRectangle()) && score >= 50 && game.getState() == 2) {

                    JOptionPane.showMessageDialog(null, "With "
                            + (score) + " pts. on the board...", "You crashed!", 1);

                    game.setDifficulty(startDifficulty);
                    gotoTitle();
                }
            }

            for (int i = 0; i < bullets.size(); i++)
                for (int j = 0; j < aliens.size(); j++)
                    if (bullets.get(i).getRectangle().intersects(aliens.get(j).getFireRectangle()) && game.getState() == 2) {

                        score += 100;

                        bullets.remove(i);
                        aliens.remove(j);
                    }

        } catch (Exception e) {}
    }

    private void gotoTitle() {

        setGamePaused(false);

        player.setDx(0);
        player.setDy(0);

        if (score > bestScore)
            bestScore = score;

        score = 0;

        game.setState(1);
    }

    public void paint(Graphics g) {

        Graphics2D g2D = (Graphics2D) g;

        background.draw(g2D);

        if (game.getState() == 1) {

            g2D.setColor(game.getMenuColor());
            g2D.drawString("Enemy max speed: " + speed, 1140, 690);
            g2D.setFont(font);
            g2D.drawString("Current attempt's best score: " + bestScore, 460, 480);

            if (arcade)
                g2D.drawString("Current game type: Arcade", 460, 20);
            else g2D.drawString("Current game type: Simulator", 460, 20);;
        }

        if (loadScores) {

            try {
                g2D.drawString("Best score by {" + serialize.readName() +
                        "} |" + serialize.readDifficulty() +
                        " & Enemy Speed: " + serialize.readSpeed() + " & Game Mode: " + serialize.readMode() + "| ["
                        + serialize.readTime() + "]: " + serialize.readScore(), 4, 690);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        Iterator<Alien> alienIterator = aliens.iterator();

        while (alienIterator.hasNext()) {

            Alien alien = alienIterator.next();

            if (alien.getY() >= 784) {

                alienIterator.remove();
                if (score >= 50) score -= 10;
            }

            else {

                if (!gamePaused)
                    alien.move();

                alien.draw(g2D);
            }
        }

        Iterator<Bullet> bulletIterator = bullets.iterator();

        while (bulletIterator.hasNext()) {

            Bullet bullet = bulletIterator.next();

            if (bullet.getY() <= -25) bulletIterator.remove();

            else {

                if (!gamePaused)
                    bullet.move();

                bullet.draw(g2D);
            }
        }

        game.draw(g2D);
        game.setScore(score);
    }
}
