package ga.exdev.x1.game;

import javax.swing.ImageIcon;
import java.awt.*;
import java.awt.image.ImageObserver;

public class Alien {

    private Image image = new ImageIcon(getClass().getResource("/alien.png")).getImage();
    private ImageObserver imageObserver = new ImageObserver() {
        public boolean imageUpdate(Image image, int infoflags, int x, int y, int width, int height) {
            return false;
        }
    };

    private int x;
    private int y;
    private int v;

    private Player player;

    //Getters
    public int getY() {

        return y;
    }
    public Rectangle getFireRectangle() {

        return new Rectangle(x, y, image.getWidth(imageObserver), image.getHeight(imageObserver));
    }
    public Rectangle getRectangle() {

        return new Rectangle(x, y, image.getWidth(imageObserver)-10, image.getHeight(imageObserver)-10);
    }

    public Alien (int x, int y, int v, Player player) {

        this.x = x;
        this.y = y;
        this.v = v;
        this.player = player;
    }

    public void draw(Graphics2D g) {

        g.drawImage(image, x, y, null);
    }

    public void move() {

        y += v;
    }
}
