package ga.exdev.x1.game;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;

public class Bullet {

    private Image image = new ImageIcon(getClass().getResource("/bullet.png")).getImage();
    private ImageObserver imageObserver = new ImageObserver() {
        public boolean imageUpdate(Image image, int infoflags, int x, int y, int width, int height) {
            return false;
        }
    };

    private int v;
    private int x;
    private int y;

    //Getters
    public int getY() {

        return y;
    }
    public Rectangle getRectangle() {

        return new Rectangle(x, y, image.getWidth(imageObserver), image.getHeight(imageObserver));
    }

    public Bullet (int v, int x, int y) {

        this.v = v;
        this.x = x + 25;
        this.y = y + 40;
    }

    public void draw(Graphics2D g) {

        g.drawImage(image, x, y, null);
    }

    public void move() {

        y -= v;
    }
}
