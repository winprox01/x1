package ga.exdev.x1.game;

import javax.swing.ImageIcon;
import java.awt.*;
import java.awt.image.ImageObserver;

public class Player {

    private Image image = new ImageIcon(getClass().getResource("/player.gif")).getImage();
    private ImageObserver imageObserver = new ImageObserver() {
        public boolean imageUpdate(Image image, int infoflags, int x, int y, int width, int height) {
            return false;
        }
    };

    private int x = 600;
    private int y = 1000;
    private int dx = 0;
    private int dy = 0;

    //Getters & Setters
    public void setDx(int dx) {

        this.dx = dx;
    }
    public void setDy(int dy) {

        this.dy = dy;
    }
    public int getX() {

        return x;
    }
    public int getY() {

        return y;
    }
    public Rectangle getRectangle() {

        return new Rectangle(x, y, image.getWidth(imageObserver)-10, image.getHeight(imageObserver)-10);
    }

    public void draw(Graphics2D g) {

        g.drawImage(image, x, y, null);
    }

    public void moveX() {

        if (x <= 0)
            x = 0;

        if (x >= 1214)
            x = 1214;

        x += dx;
    }
    public void moveY() {

        if (y <= 0)
            y = 0;

        if (y >= 624)
            y = 624;

        y += dy;
    }

    public void center() {

        if (y > 380)
            y -= 21;
        if (y < 380)
            y += 21;

        if (x > 590)
            x -= 21;
        if (x < 590)
            x += 21;
    }
}
